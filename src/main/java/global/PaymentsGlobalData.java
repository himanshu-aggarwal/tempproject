package global;

import com.one97.paytm.reporting.listeners.RetryAnalyzer;
import java.util.ResourceBundle;

public class PaymentsGlobalData{

    public static final String EXTENTREPORT_DOCUMENTTITLE = "Android App Automation - Digital";
    public static final String EXTENTREPORT_REPORTNAME = "Android Automation";
    public static final boolean FLAG_CAPTURESCREENSHOTS = true;
    public static final boolean FLAG_REMOVE_RETRIEDTESTS = true;

    public static String ADBPATH;
    public static final int RETRYLIMIT = 1;
    public static String ANDROID_HOME;

    public static final String FILEPATH_MASTER_PROPERTIES_FILE = "globaldata/config";
    public static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(FILEPATH_MASTER_PROPERTIES_FILE);
    public static String NODE_PATH = RESOURCE_BUNDLE.getString("NODE_PATH");
    public static String APPIUM_JS_FILE_PATH = RESOURCE_BUNDLE.getString("APPIUM_JS_FILE");
    public static final String APP_PATH = RESOURCE_BUNDLE.getString("APP_PATH");
    public static final String APP_PACKAGE = RESOURCE_BUNDLE.getString("APP_PACKAGE");
    public static final String APP_ACTIVITY = RESOURCE_BUNDLE.getString("APP_ACTIVITY");
    public static final String APP_FILE_NAME = RESOURCE_BUNDLE.getString("APP_FILE_NAME");
    public static final String APP_COMPLETE_PATH = APP_PATH + "/" + APP_FILE_NAME;

    public static String ENVIRONMENT = System.getProperty("environment", "live");
    public static String GROUP_NAMES = System.getProperty("gname", "");
    public static String EXECUTION_TYPE = System.getProperty("suitetype", "system");
    public static String EXCLUDE_GROUPS = System.getProperty("excludeGroups", "");
    public static String LANGUAGE = System.getProperty("language", "english");
    public static String APPSTAMP = "";

    public static boolean FLAG_UPDATEKLOV = Boolean.parseBoolean(System.getProperty("updateklov", "false"));
    public static boolean FLAG_UPDATEDING = Boolean.parseBoolean(System.getProperty("updateding", "false"));
    public static boolean FLAG_UPDATEJIRA = Boolean.parseBoolean(System.getProperty("updatejira", "false"));


    public static final String JIRA_USER_NAME = RESOURCE_BUNDLE.getString("JIRA_USER_NAME");
    public static final String JIRA_PASSWORD = RESOURCE_BUNDLE.getString("JIRA_PASSWORD");
    public static final String JIRA_PROJECT_NAME = RESOURCE_BUNDLE.getString("JIRA_PROJECT_NAME");
    public static final String JIRA_RELEASE_NAME_PREFIX = RESOURCE_BUNDLE.getString("JIRA_RELEASE_NAME_PREFIX");
    public static final String JIRA_TEST_REPO_RELEASE_NAME = RESOURCE_BUNDLE.getString("JIRA_TEST_REPO_RELEASE_NAME");
    public static final String JIRA_TEST_REPO_CYCLE_NAME = RESOURCE_BUNDLE.getString("JIRA_TEST_REPO_CYCLE_NAME");
    public static final String JIRA_NEW_CYCLE_NAME_PREFIX = RESOURCE_BUNDLE.getString("JIRA_NEW_CYCLE_NAME_PREFIX");


    public static final String KLOV_PROJECTNAME = "Android App Automation - Digital";
    public static final String KLOV_SERVERIP = "";
    public static final int KLOV_MONGODBPORT = 0;
    public static final int KLOV_SERVERPORT = 0;


    public static final String DING_ACCESS_TOKEN_AUTOMATIONGROUP = "a33542410a1b248a87839cbb0d8f252c4ca729cb4fa357f78d5d4560b1bd44bc";
    public static final String DING_MESSAGETITLE = "ANDROID APP AUTOMATION - DIGITAL";
    public static final int DING_THRESHOLDPERCENTAGE = 83;
    public static final String DING_ANDROIDLOGO_URL = "http://www.androupdates.com/wp-content/uploads/2016/11/AndroidLogo.jpg";
    public static final String JENKINS_REPORT_URL = System.getProperty("buildurl") + "HTML_20Report";




    static {

        if (System.getenv("NODE_PATH") != null) {
            NODE_PATH = System.getenv("NODE_PATH");
        }

        if (System.getenv("APPIUM_JS_FILE") != null) {
            APPIUM_JS_FILE_PATH = System.getenv("APPIUM_JS_FILE");
        }

        if (System.getenv("ANDROID_HOME") != null) {
            ANDROID_HOME = System.getenv("ANDROID_HOME");
        }

        ADBPATH = ANDROID_HOME + "/platform-tools/adb";
        RetryAnalyzer.setRetryLimit(RETRYLIMIT);

        System.out.println("Environment = " + ENVIRONMENT);
        System.out.println("Execution Type = " + EXECUTION_TYPE);
        System.out.println("Language = " + LANGUAGE);
        System.out.println("Retry Limit = " + RETRYLIMIT);

    }


}
