package pageobjects.addmoney;

import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.logging.Logger;
import org.openqa.selenium.By;

public class AddMoneyScreen {

    private static AddMoneyScreen instance = new AddMoneyScreen();
    private By addMoneyWallet = By.xpath("//android.widget.TextView[contains(@resource-id,'title') and contains(@text,'Paytm Wallet')]");


    private AddMoneyScreen() {
    }

    public static AddMoneyScreen getInstance() {
        Logger.logPass("On Add Money Page");
        return instance;
    }

    public void click_AddMoneyToWallet(){
        if(ActionHelper.isPresentWithWait(addMoneyWallet)){
            ActionHelper.click(addMoneyWallet);
        }
    }



}
