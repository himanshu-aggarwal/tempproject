package pageobjects.addmoney;

import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.logging.Logger;
import org.openqa.selenium.By;

public class WalletAddMoneyScreen {

    private static WalletAddMoneyScreen instance = new WalletAddMoneyScreen();
    private By addMoneyAmountTextField = By.id("amw_amount_input_et");
    private By addMoneyButton = By.id("amw_add_money_to_wallet_btn");


    private WalletAddMoneyScreen() {
    }

    public static WalletAddMoneyScreen getInstance() {
        Logger.logPass("On Wallet Add Money Page");
        return instance;
    }

    public void inputAddMoneyAmount(String amount){
        if(ActionHelper.isPresentWithWait(addMoneyAmountTextField)){
            ActionHelper.fillWithClear(addMoneyAmountTextField,amount);
        }
    }

    public void click_AddMoneyButton(){
        if(ActionHelper.isPresentWithWait(addMoneyButton)){
            ActionHelper.click(addMoneyButton);
        }
    }



}
