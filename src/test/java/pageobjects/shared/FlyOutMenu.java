package pageobjects.shared;

import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.assertions.CustomAssert;
import com.one97.paytm.reporting.logging.Logger;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.List;

public class FlyOutMenu {

    public static By myOrders_TV = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/label_tv' and @text='My Orders']/..");
    public static By loginButton_TV = By.id("net.one97.paytm:id/ln_profile_login");
    public static By profileName_TV = By.id("net.one97.paytm:id/ln_name_tv");
    public static By profileQr_TV = By.id("net.one97.paytm:id/qr");
    public static By paymentSettings_TV = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/label_tv' and @text='Payment Settings']/..");
    private static FlyOutMenu instance = new FlyOutMenu();
    public By rechargeOrPayForButton_TV = By.xpath("//android.widget.TextView[@text='Recharge or Pay for']");
    public By bookOnPaytmButton_TV = By.xpath("//android.widget.TextView[@text='Book on Paytm']");
    public By referAshopButton_TV = By.xpath("//android.widget.TextView[@text='Refer a Shop']");
    public String categoryList_FlyOutMenu_Xpath_TV = "//android.widget.FrameLayout[@resource-id='net.one97.paytm:id/flyout_frame']//android.widget.TextView[@text='xyz']";
    public String categoryList_FlyOut_Xpath_TV = "//android.widget.TextView[@resource-id='net.one97.paytm:id/label_tv' and contains(@text,'xyz')]/..";
    private By vipCashbackOffers_IV = By.id("net.one97.paytm:id/ic_profile");
    private By help_LL = By.id("net.one97.paytm:id/help_ll");
    private By shopOnPaytmButton_TV = By.xpath("//android.widget.TextView[@text='Shop on Paytm']");
    private By changePassword_LL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/item_text' and @text='Change Password']");
    private By manageAppLock_LL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/item_text' and @text='Manage App Lock']");
    private By manageAppLockToggle_Switch = By.id("net.one97.paytm:id/security_toggle_button");
    private By changePasscode_LL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/item_text' and contains(@text,'Passcode')]");
    private By paytmFirst_TV = By.xpath("//android.widget.TextView[@text='Paytm First']");
    private By userName_TV = By.id("net.one97.paytm:id/ln_name_tv");
    private By userGender_TV = By.id("");
    private By userDOB_TV = By.id("");
    private By profileSetting_TV = By.id("");

    //navigation to
    private By loader_TV = By.id("net.one97.paytm:id/wallet_loader");
    private By paymentReminders_TV = By.xpath("//android.widget.TextView[@text='Payment Reminders']");
    private By creditCard_TV = By.id("net.one97.paytm:id/creditIcon");
    private By favouriteStores_TV = By.xpath("//android.widget.TextView[@text='My Favourite Stores']");
    private By cancelButton_LocationPermission_Btn = By.id("android:id/button2");
    private By okButton_LocationPermission_Btn = By.id("android:id/button1");


    //Paytm First
    private By paytmFirstIcon_TV = By.id("net.one97.paytm:id/rel_prime_user_access");
    private By paytmFirstHeading_TV = By.xpath("//android.widget.TextView[@text='Paytm First']");
    private By paytmFirstSubHeading_TV = By.xpath("(//android.widget.TextView[@text='Paytm First'])/following-sibling::android.widget.TextView");
    private By paytmFirstImage_TV = By.id("net.one97.paytm:id/img_prime");
    private By paytmFirstArrow_TV = By.xpath("(//android.widget.TextView[@text='Paytm First'])/../following-sibling::android.widget.ImageView");
    private By paytmCreditCard_TV = By.id("net.one97.paytm.paytm_creditcard:id/creditIcon");


    //Paytm First Card
    private By paytmFirstCard_TV = By.xpath("//android.widget.FrameLayout[@resource-id='net.one97.paytm:id/flyout_frame']//android.widget.TextView[@text='Paytm First Card']");
    //private By paytmFirstCardIcon_TV = By.id("net.one97.paytm:id/rel_prime_user_access");
    private By paytmFirstCardHeading_TV = By.xpath("//android.widget.FrameLayout[@resource-id='net.one97.paytm:id/flyout_frame']//android.widget.TextView[@text='Paytm First Card']");
    private By paytmFirstCardSubHeading_TV = By.xpath("(//android.widget.FrameLayout[@resource-id='net.one97.paytm:id/flyout_frame']//android.widget.TextView[@text='Paytm First Card'])/following-sibling::android.widget.TextView");
    private By paytmFirstCardImage_TV = By.id("net.one97.paytm:id/img_prime");
    private By paytmFirstCardArrow_TV = By.xpath("(//android.widget.FrameLayout[@resource-id='net.one97.paytm:id/flyout_frame']//android.widget.TextView[@text='Paytm First Card'])/../following-sibling::android.widget.ImageView");


    public static FlyOutMenu getInstance() {
        Logger.logPass("On FlyOutMenu Menu ");
        return instance;
    }

    public void click_LoginButton_TV() {
        if (ActionHelper.isPresent(loginButton_TV)) {
            ActionHelper.click(loginButton_TV);
            ActionHelper.waitForPageProgress();
        }
    }

    public void click_MyOrders_TV() {
        ActionHelper.gotoSleep(1000);
        ActionHelper.scrollTillElementVerySlowly(myOrders_TV);
        ActionHelper.click(myOrders_TV);
        ActionHelper.waitForPageProgress();
        //click_CategoryList_FlyOut_TV("Shopping Orders");
        ActionHelper.waitForPageProgress();
        SharedActions.getInstance().click_Close_SecurePatternLayer_Btn();
    }

    public void validatePaymentSettings_Logout() {
        ActionHelper.gotoSleep(1000);
        ActionHelper.scrollTillElementVerySlowly(paymentSettings_TV);
        boolean flag = ActionHelper.isPresent(paymentSettings_TV);
        if (flag) {
            CustomAssert.assertFail("Able to Find Payment Setting in Logout");

        }
    }

    public boolean isDisplayed_ProfileImage_IV() {
        return ActionHelper.isPresent(profileName_TV);
    }

    public void click_ProfileName_TV() {
        ActionHelper.click(profileName_TV);
    }

    public void click_VipCashbackOffers_IV() {
        ActionHelper.click(vipCashbackOffers_IV);
    }

    public void click_Help_LL() {
        click_CategoryList_FlyOut_TV("24x7 Help");

    }

    public void click_LabelFromFlyout(String lableName) {
        ActionHelper.waitForPageProgress();
        click_CategoryList_FlyOut_TV(lableName);

    }

    public void click_RechargeOrPayForButton_TV() {
        ActionHelper.scrollTillElementVerySlowly(rechargeOrPayForButton_TV);
        ActionHelper.click(rechargeOrPayForButton_TV);
    }

    public void click_BookOnPaytmButton_TV() {
        ActionHelper.scrollTillElementVerySlowly(bookOnPaytmButton_TV);
        ActionHelper.click(bookOnPaytmButton_TV);
    }

    public void click_ReferAshopButton_TV() {
        ActionHelper.scrollTillElementVerySlowly(referAshopButton_TV);
        ActionHelper.click(referAshopButton_TV);
    }

    public void click_ShopOnPaytmButton_TV() {
        ActionHelper.scrollTillElementVerySlowly(shopOnPaytmButton_TV);
        ActionHelper.click(shopOnPaytmButton_TV);
    }

    public void click_CategoryList_FlyOutMenu_TV(String categoryName) {
        By by = By.xpath(categoryList_FlyOutMenu_Xpath_TV.replace("xyz", categoryName));
        List<MobileElement> me = ActionHelper.findElements(by);
        if (me.size() > 1)
            me.get(1).click();
        else
            me.get(0).click();
    }

    public void click_CategoryList_FlyOut_TV(String categoryName) {
        By by = By.xpath(categoryList_FlyOut_Xpath_TV.replace("xyz", categoryName));
        ActionHelper.scrollTillElementVerySlowly(by);
        List<MobileElement> me = ActionHelper.findElements(by);
        if (me.size() > 1)
            me.get(1).click();
        else
            me.get(0).click();
        ActionHelper.waitForPageProgress();
    }

    public boolean isPresent_ManageAppLockToggle_Switch() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(manageAppLockToggle_Switch);
    }

    public void click_ChangePasscode_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(changePasscode_LL);
        ActionHelper.click(changePasscode_LL);
    }

    public void click_ChangePassword_TV() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(changePassword_LL);
        ActionHelper.click(changePassword_LL);
        ActionHelper.waitForPageProgress();
    }

    public void click_ManageAppLock_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(manageAppLock_LL);
        ActionHelper.click(manageAppLock_LL);
    }

    public void click_PaytmFirst_FromFlyout() {
        ActionHelper.click(paytmFirst_TV);
    }

    public void click_PaytmFirstIcon_TV() {
        ActionHelper.click(paytmFirstIcon_TV);
    }

    public boolean isDisplayed_PaytmFirstHeading_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(paytmFirstHeading_TV);
    }

    public boolean isDisplayed_PaytmFirstSubHeading_TV() {
        return ActionHelper.isPresent(paytmFirstSubHeading_TV);
    }

    public String getText_PaytmFirstSubHeading_TV() {
        return ActionHelper.getText(paytmFirstSubHeading_TV);
    }

    public boolean isDisplayed_PaytmFirstImage_TV() {
        return ActionHelper.isPresent(paytmFirstImage_TV);
    }

    public boolean isDisplayed_PaytmFirstArrow_TV() {
        return ActionHelper.isPresent(paytmFirstArrow_TV);
    }

    public boolean isDisplayed_PaytmFirstIcon_TV() {
        return ActionHelper.isPresent(paytmFirstIcon_TV);
    }

    //Paytm First Card

    public void click_PaytmFirstCard_FromFlyout() {
        ActionHelper.waitForPageProgress();
        ActionHelper.click(paytmFirstCard_TV);
        ActionHelper.waitForPageProgress();
    }


    public boolean isDisplayed_PaytmFirstCardHeading_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(paytmFirstCardHeading_TV);
    }

    public String getText_PaytmFirstCardHeading_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.getText(paytmFirstCardHeading_TV);
    }

    public String getText_PaytmFirstCardSubHeading_TV() {
        return ActionHelper.getText(paytmFirstCardSubHeading_TV);
    }

    public boolean isDisplayed_PaytmFirstCardImage_TV() {
        return ActionHelper.isPresent(paytmFirstCardImage_TV);
    }

    public boolean isDisplayed_PaytmFirstCardArrow_TV() {
        return ActionHelper.isPresent(paytmFirstCardArrow_TV);
    }

    public String getText_UserName_TV() {
        return ActionHelper.getText(userName_TV);

    }

    public String getText_UserName_TV_new() {
        return ActionHelper.getText(userName_TV);

    }



/*
   public void click_PaytmFirstCardIcon_TV() {
        ActionHelper.click(paytmFirstCardIcon_TV);
    }

    public boolean isDisplayed_PaytmFirstCardIcon_TV() {
        return ActionHelper.isPresent(paytmFirstCardIcon_TV);
    }
*/
    ////

    public boolean isDisplayed_PaymentReminders_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(paymentReminders_TV);
    }

    public boolean isDisplayed_CreditCard_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(creditCard_TV);
    }

    public boolean isDisplayed_FavouriteStores_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(favouriteStores_TV);
    }

    public void click_profileQr_TV() {
        ActionHelper.click(profileQr_TV);
        ActionHelper.waitForPageProgress();
    }

    public void click_CancelButton_LocationPermission_Btn() {
        ActionHelper.gotoSleep(2000);
        if (ActionHelper.isPresent(cancelButton_LocationPermission_Btn))
            ActionHelper.click(cancelButton_LocationPermission_Btn);
    }

    public void click_okButton_LocationPermission_Btn() {
        ActionHelper.gotoSleep(1000);
        if (ActionHelper.isPresent(okButton_LocationPermission_Btn))
            ActionHelper.click(okButton_LocationPermission_Btn);
        SharedActions.getInstance().click_AllowButton_PermissionDialogue_Btn();
    }

    public boolean isDisplayed_PsaytmCreditCard_TV(){
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(paytmCreditCard_TV);
    }

    /// KYC details for Paytm First Card

    public String[] getText_UserDetails_TV() {
        ActionHelper.waitForPageProgress();
        ActionHelper.click(profileSetting_TV);
        ActionHelper.waitForPageProgress();
        ActionHelper.scrollPageUp();
        ActionHelper.waitForPageProgress();


        String fullName = ActionHelper.getText(userName_TV);;
        Logger.logPass("Full name is :" + fullName );

        String nameArray[] = fullName.split("\\s+");
        int arrlength = nameArray.length;
        String firstName= nameArray[0];
        String lastName= nameArray[arrlength-1];

        String userDetails[] =  new String[4];
        userDetails[0]= firstName;
        userDetails[1]= lastName;
        userDetails[2]= ActionHelper.getText(userGender_TV);
        userDetails[3] = ActionHelper.getText(userDOB_TV);

        return userDetails;
    }


}
