package pageobjects.shared;

import com.one97.paytm.appautomation.driver.DriverManager;
import com.one97.paytm.appautomation.global.GlobalData;
import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.assertions.CustomAssert;
import com.one97.paytm.reporting.exception.SkipTestException;
import com.one97.paytm.reporting.logging.Logger;
import global.PaymentsGlobalData;
import org.openqa.selenium.By;
import pageobjects.home.HomeScreen;
import pageobjects.login.LoginScreen;
import pageobjects.profile.ChooseLanguage_Screen;
import pageobjects.profile.ProfileScreen;

public class SharedActions {

    public static boolean retryOrder = false;
    private static SharedActions instance = null;
    private By close_SecurePatternLayer_Btn = By.id("net.one97.paytm.passbook:id/iv_close_icon");
    private By okButton_ConnectionErrorLayer_Btn = By.xpath("//android.widget.Button[@resource-id='net.one97.paytm:id/w_custom_dialog_btn_positive' and @text='OK']");
    private By allowButton_PermissionDialogue_Btn = By.id("com.android.packageinstaller:id/permission_allow_button");
    private By okButton_LocationPermissionDialogue_Btn = By.id("android:id/button1");
    private By cancelButton_LocationPermissionDialogue_Btn = By.id("android:id/button2");
    private By agree_RootedDeviceAlert_btn = By.xpath("//android.widget.Button[@resource-id='net.one97.paytm:id/w_custom_dialog_btn_positive' and @text='I Agree']");
    private By crossButton_BankLayer_TV = By.id("net.one97.paytm:id/iv_close");
    private By popUpOkButton_Btn = By.id("net.one97.paytm:id/w_custom_dialog_btn_positive");
    private By popUpMessage_TV = By.id("net.one97.paytm:id/w_custom_dialog_message");
    private By help_TV = By.xpath("//android.widget.TextView[@text='Help']/..");
    private By videoThumbnail_IV = By.id("net.one97.paytm:id/video_thumbnail");
    private By videoTitle_TV = By.id("net.one97.paytm:id/video_title");
    private By videoPlayButton_Btn = By.id("net.one97.paytm:id/video_button");
    private By deleteRecents_TV = By.id("net.one97.paytm:id/clear_option");
    private By cancelOption_TV = By.id("net.one97.paytm:id/cancel_option");
    private By recentsPayBills_TV = By.id("net.one97.paytm:id/txt_recharge_amount");
    private By showMoreButton_Btn = By.id("net.one97.paytm:id/show_more_btn");
    private By offerName_TV = By.id("net.one97.paytm:id/offer_item_code");
    private By offerDescription_TV = By.id("net.one97.paytm:id/offer_item_hint_text");
    private By offerArrow_IV = By.id("net.one97.paytm:id/offer_item_hint_text");
    private By offerTab_TV = By.xpath("//android.widget.TextView[@text='Offers']");
    private By RecentsTab_TV = By.xpath("//android.widget.TextView[@text='Recents']");
    private By subscriberIdValidationMsg_ET = By.id("net.one97.paytm:id/message_input_field");
    private By operatorGridView_GV = By.id("net.one97.paytm:id/operator_grid_view");
    private By monthly_Plan_TV = By.id("net.one97.paytm:id/description_text");
    private By recentRecharge_TV = By.id("net.one97.paytm:id/txt_recharge_amount");
    private By sessionTimeOutMessage_TV = By.xpath("//*[contains(@text,'Session Time Out') or contains(@text,'Login Required') or contains(@text,'Login required') or contains(@text,'SSO Token')]");

    //Recent content
    private By recentOperatorIcon_IV = By.id("net.one97.paytm:id/operator_icon");
    private By caNumber_RecentOrder_TV = By.id("net.one97.paytm:id/txt_mobile_number");
    private By payBill_RecentOrder_TV = By.id("net.one97.paytm:id/txt_recharge_amount");
    private By offerDetails_TV = By.id("net.one97.paytm:id/offer_details_text");
    private By lastPaidDate_TV = By.id("net.one97.paytm:id/txt_service_provider");


    public static SharedActions getInstance() {
        if (instance == null) {
            instance = new SharedActions();
        }
        return instance;
    }

    public boolean isDisplayed_SessionTimeOutMessage_TV() {
        return ActionHelper.isPresent(sessionTimeOutMessage_TV);
    }

    public void checkSessionAndPerformLogin() {
        try {
            if (isDisplayed_SessionTimeOutMessage_TV()) {
                System.out.println("========= Session is logged out Relogging now =======");
                Logger.logDebug("========= Session is logged out Relogging now =======");
                performCommonActions_Logout();
                performCommonActions();
            }
        } catch (Exception e) {
        }
    }

    public void checkAndPerformLogin() {
        if (!DriverManager.getLoggedIn()) {

            if (ActionHelper.isPresent(HomeScreen.flyoutButton_TV)) {
                HomeScreen.getInstance().click_FlyoutButton_TV();
                FlyOutMenu.getInstance().click_LoginButton_TV();
            }

            if (LoginScreen.getInstance().isDisplayed_LoginPaytm_Btn() || LoginScreen.getInstance().isDisplayed_Email_ET()) {
                performingLogin();
            } else {
                ActionHelper.navigateBack();
                DriverManager.setLoggedIn(true);
            }
        } else {
            if (LoginScreen.getInstance().isDisplayed_LoginPaytm_Btn() || LoginScreen.getInstance().isDisplayed_Email_ET())
                performingLogin();
        }
    }


    public void performingLogin() {
        click_AllowButton_PermissionDialogue_Btn();
        Logger.logPass("Performing Login on Device " + DriverManager.getDeviceSerial() + " :<br><b>MobNo = </b>" + DriverManager.getMobileNumber()
                + "<br><b>Password = </b>" + DriverManager.getPassword());
        System.out.println("Performing Login on Device = " + DriverManager.getDeviceSerial() + ", MobNo = " + DriverManager.getMobileNumber()
                + ", Password = " + DriverManager.getPassword() + System.lineSeparator());
        LoginScreen.getInstance().performLogin(DriverManager.getMobileNumber(), DriverManager.getPassword());
        DriverManager.setLoggedIn(true);
    }

    public void performCommonActions() {
        ActionHelper.gotoSleep(500);
        if (isDisplayed_SessionTimeOutMessage_TV()) {
            checkSessionAndPerformLogin();
        } else {
            click_HandleAllHomePopup_TV();
            checkAndPerformLogin();
            click_HandleAllHomePopup_TV();
        }
    }

    public void performCommonActions_Logout() {
        ActionHelper.resetApp();
        click_HandleAllHomePopup_TV();
        ChooseLanguage_Screen.getInstance().click_EnglishLanguage_TV();
        ChooseLanguage_Screen.getInstance().click_ContinueButton_Btn();
        ActionHelper.gotoSleep(2000);
        click_HandleAllHomePopup_TV();
        LoginScreen.getInstance().click_doItLaterButton_TV();
        ActionHelper.gotoSleep(2000);
        click_HandleAllHomePopup_TV();
    }

    public void performLogout() {
        ActionHelper.launchApp();
        HomeScreen.getInstance().click_FlyoutButton_TV();
        FlyOutMenu.getInstance().click_ProfileName_TV();
        ProfileScreen.getInstance().click_LogoutButton_Btn();

    }

    public void click_Close_SecurePatternLayer_Btn() {
        ActionHelper.waitForPageProgress();
        if (ActionHelper.isPresent(close_SecurePatternLayer_Btn))
            ActionHelper.click(close_SecurePatternLayer_Btn);
    }

    public void click_OkButton_ConnectionErrorLayer_Btn() {
        if (ActionHelper.isPresent(okButton_ConnectionErrorLayer_Btn))
            ActionHelper.click(okButton_ConnectionErrorLayer_Btn);
    }

    public void click_Agree_RootedDeviceAlert_btn() {
        while (ActionHelper.isPresent(agree_RootedDeviceAlert_btn)) {
            ActionHelper.click(agree_RootedDeviceAlert_btn);
            ActionHelper.gotoSleep(500);
        }
    }

    public void click_CancelButton_LocationPermissionDialogue_Btn() {
        if (ActionHelper.isPresent(cancelButton_LocationPermissionDialogue_Btn))
            ActionHelper.click(cancelButton_LocationPermissionDialogue_Btn);
    }

    public void click_OkButton_LocationPermissionDialogue_Btn() {
        if (ActionHelper.isPresent(okButton_LocationPermissionDialogue_Btn))
            ActionHelper.click(okButton_LocationPermissionDialogue_Btn);
    }

    public void click_CrossButton_BankLayer_TV() {
        if (ActionHelper.isPresent(crossButton_BankLayer_TV))
            ActionHelper.click(crossButton_BankLayer_TV);
    }

    public void click_AllowButton_PermissionDialogue_Btn() {
        ActionHelper.hideKeyboard();
        if (!ActionHelper.isPresent(allowButton_PermissionDialogue_Btn))
            return;
        while (ActionHelper.isPresent(allowButton_PermissionDialogue_Btn)) {
            System.out.println("Granting Permissions !!");
            click_AllowPermission_Btn();
            ActionHelper.gotoSleep(500);
        }
    }

    public void click_AllowPermission_Btn() {
        try {
            ActionHelper.click(allowButton_PermissionDialogue_Btn);
        } catch (Exception e) {
        }
    }

    public void click_HandleAllHomePopup_TV() {
        click_OkButton_ConnectionErrorLayer_Btn();
        click_Agree_RootedDeviceAlert_btn();
        click_CrossButton_BankLayer_TV();
        click_AllowButton_PermissionDialogue_Btn();
        click_AcceptAllPermission_Alert();
    }

    private void click_AcceptAllPermission_Alert() {
        try {
            if (!ActionHelper.isAlertPresent())
                return;
            while (ActionHelper.isAlertPresent()) {
                Logger.logDebug("Granting Permissions By Alert !!");
                ActionHelper.acceptAlert();
                ActionHelper.gotoSleep(500);
            }
        } catch (Exception e) {
            Logger.logDebug("Exception in Granting Permission ::");
        }
    }

    public void fetchAndSetAppVersion_Stamp() {
        try {
            ActionHelper.launchApp();
            performCommonActions();
            HomeScreen.getInstance().click_FlyoutButton_TV();
            FlyOutMenu.getInstance().click_ProfileName_TV();
            PaymentsGlobalData.APPSTAMP = ProfileScreen.getInstance().getText_AppVersion_TV() + " " + ProfileScreen.getInstance().getText_AppStamp_TV();
        } catch (Exception e) {
            System.out.println("XXXXX Unable to Fetch App Stamp XXXXX`");
        }
    }

    public void click_PopUpOkButton_Btn() {
        ActionHelper.click(popUpOkButton_Btn);
    }

    public boolean isPresent_PopUpOkButton_Btn() {
        return ActionHelper.isPresent(popUpOkButton_Btn);
    }

    public String getText_popUpMessage_TV() {
        return ActionHelper.getText(popUpMessage_TV);
    }

    public void click_Help_TV() {
        ActionHelper.waitForPageProgress(By.id("net.one97.paytm:id/loading_threedots_lav"));
        ActionHelper.scrollTillElementSlowly(help_TV);
        ActionHelper.click(help_TV);
        ActionHelper.scrollPageDownSlowly();
    }

    public void click_VideoPlayButton_Btn() {
        ActionHelper.scrollTillElementSlowly(videoPlayButton_Btn);
        ActionHelper.click(videoPlayButton_Btn);
    }

    public boolean isPresent_VideoTitle_TV() {
        ActionHelper.scrollTillElementSlowly(videoTitle_TV);
        return ActionHelper.isPresent(videoTitle_TV);
    }

    public boolean isPresent_VideoThumbnail_IV() {
        ActionHelper.scrollTillElementSlowly(videoThumbnail_IV);
        return ActionHelper.isPresent(videoThumbnail_IV);
    }

    public boolean isPresent_ShowMoreButton_Btn() {
        return ActionHelper.isPresent(offerTab_TV);
    }

    public void click_ShowMoreButton_Btn() {
        ActionHelper.scrollPageDown();
        int offersLabel = ActionHelper.findElements(offerName_TV).size();
        if (ActionHelper.isPresent(showMoreButton_Btn)) {
            CustomAssert.assertTrue(offersLabel == 5, "Validate the No of Offers Present is Five");
            ActionHelper.click(showMoreButton_Btn);
            offersLabel = ActionHelper.findElements(offerName_TV).size();
            CustomAssert.assertTrue(offersLabel > 5, "Validate the No of Offers Present is more than Five");
        } else {
            CustomAssert.assertTrue(offersLabel <= 5, "Validate the No of Offers Present Equal or less than Five");
        }
    }

    public String getText_OfferName_TV() {
        return ActionHelper.getText(offerName_TV);
    }

    public void click_OfferTab_TV() {
        ActionHelper.hideKeyboard();
        ActionHelper.gotoSleep(500);
        ActionHelper.scrollTillElementVerySlowly(offerTab_TV);
        ActionHelper.click(offerTab_TV);
        ActionHelper.waitForPageProgress();
    }

    public boolean isPresent_OfferName_TV() {
        ActionHelper.scrollPageDown();
        return ActionHelper.isPresent(offerName_TV);
    }

    public boolean isPresent_recentsPayBills_TV() {
        ActionHelper.waitForPageProgress();
        ActionHelper.scrollTillElement(recentsPayBills_TV);
        return ActionHelper.isPresent(recentsPayBills_TV);
    }

    public void click_RecentOperatorIcon_IV() {
        ActionHelper.click(recentOperatorIcon_IV);
    }

    public boolean isDisplayed_RecentOperatorIcon_IV() {
        ActionHelper.scrollTillElementSlowly(recentOperatorIcon_IV);
        return ActionHelper.isPresent(recentOperatorIcon_IV);
    }

    public void click_DeleteRecents_TV() {
        ActionHelper.click(deleteRecents_TV);
    }

    public boolean isPresent_DeleteRecents_TV() {
        ActionHelper.scrollTillElementSlowly(deleteRecents_TV);
        return ActionHelper.isPresent(deleteRecents_TV);
    }

    public boolean isPresent_RecentCancelOption_TV() {
        return ActionHelper.isPresent(cancelOption_TV);
    }

    public void click_RecentCancelOption_TV() {
        ActionHelper.click(cancelOption_TV);
    }

    public boolean isPresent_operatorGridView_GV() {
        return ActionHelper.isPresent(operatorGridView_GV);
    }

    public boolean isPresent_RecentRecharge_TV() {
        return ActionHelper.isPresent(recentRecharge_TV);
    }


    public void performLogin() {
        LoginScreen.getInstance().click_loginPaytm_Btn();
        LoginScreen.getInstance().performLogin(DriverManager.getMobileNumber(), DriverManager.getPassword());
    }

    public String getText_CaNumber_RecentOrder_TV() {
        return ActionHelper.getText(caNumber_RecentOrder_TV);
    }

    public String click_PayBill_RecentOrder_TV() {
        ActionHelper.gotoSleep(1000);
        ActionHelper.scrollTillElement(payBill_RecentOrder_TV);
        if (ActionHelper.isPresent(payBill_RecentOrder_TV)) {
            String caNumber = getText_CaNumber_RecentOrder_TV();
            ActionHelper.click(payBill_RecentOrder_TV);
            return caNumber;
        } else {
            throw new SkipTestException("Marking Test as Skip : Unable to find Recent Order");
        }
    }

    public boolean isDisplayed_OfferDescription_TV() {
        return ActionHelper.isPresent(offerDescription_TV);
    }

    public boolean isDisplayed_OfferArrow_TV() {
        return ActionHelper.isPresent(offerArrow_IV);
    }

    public void click_OfferArrow_TV() {
        ActionHelper.click(offerArrow_IV);
    }

    public boolean isDisplayed_OfferDetails_TV() {
        return ActionHelper.isPresent(offerDetails_TV);
    }

    public String isSelected_RecentsTab_TV() {
        return ActionHelper.getAttribute(RecentsTab_TV, "selected");
    }

    public String getText_LastPaidDate_TV() {
        return ActionHelper.getText(lastPaidDate_TV);
    }


}
