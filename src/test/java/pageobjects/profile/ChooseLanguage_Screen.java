package pageobjects.profile;

import com.one97.paytm.appautomation.utils.ActionHelper;
import org.openqa.selenium.By;
import pageobjects.shared.SharedActions;

public class ChooseLanguage_Screen {

    private static ChooseLanguage_Screen instance = new ChooseLanguage_Screen();

    private By english_TV = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/txt_language_vernacular' and @text='English']");
    private By continueButton_Btn = By.id("net.one97.paytm:id/btn_language_continue_button");
    private By hindi_TV = By.id("net.one97.paytm:id/txt_language_english");
    private By screenTitle_TV = By.id("net.one97.paytm:id/screen_title");
    private By languageSetupCompleted = By.xpath("//android.widget.TextView[contains(@text,'Language has been changed') or contains(@text,'Language Setup Completed')]");
    private By languageSetupFailed = By.xpath("//*[contains(@text,'Language change failed') or contains(@text,'Language changed failed') or contains(@text,'Please check your internet connection')]");

    public static ChooseLanguage_Screen getInstance() {
        return instance;
    }

    public void click_English_TV() {
        ActionHelper.scrollPageUp();
        ActionHelper.click(english_TV);
    }

    public void click_EnglishLanguage_TV() {
        try {
            ActionHelper.click(english_TV);
        }
        catch (Exception e){
            SharedActions.getInstance().click_HandleAllHomePopup_TV();
            ActionHelper.click(english_TV);
        }
    }

    public void click_Hindi_TV() {
        ActionHelper.click(hindi_TV);
    }

    public String getText_ScreenTitle_TV() {
        return ActionHelper.getText(screenTitle_TV);
    }

    public String getText_ContinueButton_Btn() {
        return ActionHelper.getText(continueButton_Btn);
    }

    public void click_ContinueButton_Btn() {
        ActionHelper.gotoSleep(1000);
        int i=0;
        while (ActionHelper.isPresent(continueButton_Btn) && i<5) {
            ActionHelper.click(continueButton_Btn);
            ActionHelper.gotoSleep(500);
            if(ActionHelper.isPresent(languageSetupCompleted)){
                break;
            }
            if(ActionHelper.isPresent(languageSetupFailed)){
                ActionHelper.waitForPageProgress(languageSetupFailed);
            }
            i++;
        }

    }

}
