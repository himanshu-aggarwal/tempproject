package pageobjects.profile;

import com.one97.paytm.appautomation.utils.ActionHelper;
import org.openqa.selenium.By;
import pageobjects.login.LoginScreen;

public class ProfileScreen {

    private static ProfileScreen instance = new ProfileScreen();
    private By profileAppFeedback_TV = By.id("net.one97.paytm:id/app_version");
    private By profileYourOrders_TV = By.id("net.one97.paytm:id/lyt_your_orders");
    private By changeLanguage_TV = By.id("net.one97.paytm:id/lyt_language_settings");
    private By profileSignOutButton_TV = By.id("net.one97.paytm:id/txt_sign_out");
    private By appVersion_TV = By.id("net.one97.paytm:id/app_version");
    private By appStamp_TV = By.id("net.one97.paytm:id/app_stamp");
    private By contactHelp_LL = By.id("net.one97.paytm:id/account_contact_us");
    private By profileScreenTitle_TV = By.id("net.one97.paytm:id/text1");
    private By myLuckyLifafas_RL = By.id("net.one97.paytm:id/lyt_my_lucky_lifafas");
    private By screenTitle_TV = By.id("net.one97.paytm:id/text1");
    private By screenHeading_TV=By.xpath("//android.widget.RelativeLayout[@resource-id='net.one97.paytm:id/header']//android.widget.TextView");
    private By myOrders_RL = By.id("net.one97.paytm:id/lyt_your_orders");
    private By qrCode_IV = By.id("net.one97.paytm:id/e_qr_image");
    private By nameProfile_TV = By.id("net.one97.paytm:id/e_name");
    private By savedItems_RL = By.id("net.one97.paytm:id/lyt_wish_list");
    private By deliveryAddress_RL = By.id("net.one97.paytm:id/lyt_delivery_address");
    private By savedPayments_RL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/item_text' and contains(@text,'Settings')]");
    private By alldeliveryAddress_RL = By.id("net.one97.paytm:id/lyt_address");
    private By addNewButton_RL = By.xpath("//android.widget.TextView[contains(@text,'ADD NEW')]");
    private By automaticPayments_RL = By.id("");
    private By linkedApps_RL = By.xpath("//android.widget.TextView[contains(@text,'Subscriptions')]");
    private By appsLinkedWithWallet_RL = By.id("net.one97.paytm:id/lyt_linked_merchant");
    private By changePassword_LL = By.id("net.one97.paytm:id/lyt_security_change_password");
    private By manageAppLock_LL = By.id("net.one97.paytm:id/lyt_security");
    private By manageAppLockToggle_Switch = By.id("net.one97.paytm:id/security_toggle_button");
    private By changePasscode_LL = By.id("net.one97.paytm:id/lyt_security_change_passcode");
    private By logoutfromAllDevices_LL = By.id("net.one97.paytm:id/lyt_security_logout_all_devices");
    private By popUp_logoutButton_Btn = By.id("net.one97.paytm:id/bottomsheetbutton_logout");
    private By loginToPaytmButton_Btn = By.id("net.one97.paytm:id/profile_login");
    private By notification_LL = By.id("net.one97.paytm:id/lyt_notification_settings");
    private By invite_LL = By.id("net.one97.paytm:id/lyt_invite");
    private By inviteToPaytm_IV = By.id("android:id/icon");
    private By chooseLanguage_LL = By.id("net.one97.paytm:id/lyt_language_settings");
    private By help_LL = By.id("net.one97.paytm:id/account_contact_us");
    private By editProfile_RL = By.id("net.one97.paytm:id/lyt_edit_profile");
    private By logoutButton_Btn = By.id("net.one97.paytm:id/lyt_sign_out");
    private By savedCardname_TV=By.id("net.one97.paytm:id/txt_card_name");
    private String paymentSettingsLabel_Xpath = "//android.widget.TextView[contains(@text,'xyz')]";

    public static ProfileScreen getInstance() {
        return instance;
    }

    public void click_ChooseLanguage_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(chooseLanguage_LL);
        ActionHelper.click(chooseLanguage_LL);
    }

    public void click_help_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(help_LL);
        ActionHelper.click(help_LL);
    }

    public void click_ProfileAppFeedback_TV() {
        ActionHelper.click(profileAppFeedback_TV);
    }

    public void click_ProfileYourOrders_TV() {
        ActionHelper.click(profileAppFeedback_TV);
    }

    public void click_ProfileSignOutButton_TV() {
        ActionHelper.click(profileSignOutButton_TV);
    }

    public void click_MyOrders_RL() {
        ActionHelper.scrollTillElementVerySlowly(myOrders_RL);
        ActionHelper.click(myOrders_RL);
    }

    public void click_DeliveryAddress_RL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(deliveryAddress_RL);
        ActionHelper.click(deliveryAddress_RL);
        ActionHelper.waitForPageProgress();
    }

    public void click_ChangePassword_TV() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(changePassword_LL);
        ActionHelper.click(changePassword_LL);
        ActionHelper.waitForPageProgress();
    }

    public void click_PaymentSettingsLabel(String label) {
        By by = By.xpath(paymentSettingsLabel_Xpath.replace("xyz", label));
        ActionHelper.scrollTillElementVerySlowly(by);
        ActionHelper.click(by);
        ActionHelper.waitForPageProgress();
    }

    public void click_SavedPayments_RL() {
        ActionHelper.click(savedPayments_RL);
    }

    public boolean isPresent_LoginToPaytmButton_Btn() {
        return ActionHelper.isPresent(loginToPaytmButton_Btn) || LoginScreen.getInstance().isDisplayed_LoginPaytm_Btn();
    }

    public void click_LoginToPaytmButton_Btn() {
        ActionHelper.click(loginToPaytmButton_Btn);
    }

    public void click_ManageAppLock_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(manageAppLock_LL);
        ActionHelper.click(manageAppLock_LL);
    }

    public void click_LogoutfromAllDevices_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(logoutfromAllDevices_LL);
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.click(logoutfromAllDevices_LL);
    }

    public void click_Notification_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(notification_LL);
        ActionHelper.click(notification_LL);
        ActionHelper.waitForPageProgress();
    }

    public void click_Invite_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(invite_LL);
        ActionHelper.click(invite_LL);
    }

    public void click_ChangePasscode_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(changePasscode_LL);
        ActionHelper.click(changePasscode_LL);
    }

    public void click_LinkedApps_RL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(linkedApps_RL);
        ActionHelper.click(linkedApps_RL);
        ActionHelper.click(appsLinkedWithWallet_RL);
    }

    public void click_AutomaticPayments_RL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(automaticPayments_RL);
        ActionHelper.click(automaticPayments_RL);
    }

    public void click_SavedItems_RL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElementVerySlowly(savedItems_RL);
        ActionHelper.click(savedItems_RL);
    }

    public boolean isPresent_inviteToPaytm_IV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(inviteToPaytm_IV);
    }

    public boolean check_QrCode_IV() {
        return ActionHelper.isPresent(qrCode_IV);
    }

    public boolean check_NameProfile_TV() {
        return ActionHelper.isPresent(nameProfile_TV);
    }

    public String getText_NameProfile_TV() {
        return ActionHelper.getText(nameProfile_TV);
    }

    public void click_ChangeLanguage_TV() {
        ActionHelper.click(changeLanguage_TV);
    }

    public void click_popUp_logoutButton_Btn() {
        ActionHelper.click(popUp_logoutButton_Btn);
    }

    public void click_MyLuckyLifafas_RL() {
        ActionHelper.scrollTillElementVerySlowly(myLuckyLifafas_RL);
        ActionHelper.click(myLuckyLifafas_RL);
    }

    public void click_ContactHelp_LL() {
        ActionHelper.scrollPageDownSlowly();
        ActionHelper.scrollTillElement(contactHelp_LL);
        ActionHelper.click(contactHelp_LL);
    }

    public boolean isPresent_ManageAppLockToggle_Switch() {
        return ActionHelper.isPresent(manageAppLockToggle_Switch);
    }

    public boolean check_AlldeliveryAddress_RL() {
        return ActionHelper.isPresent(alldeliveryAddress_RL);
    }

    public boolean check_AddNewButton_RL() {
        return ActionHelper.isPresent(addNewButton_RL);
    }

    public String getText_AppVersion_TV() {
        ActionHelper.scrollTillElement(appVersion_TV);
        return ActionHelper.getText(appVersion_TV);
    }

    public String getText_AppStamp_TV() {
        ActionHelper.scrollTillElement(appStamp_TV);
        return ActionHelper.getText(appStamp_TV);
    }

    public String getText_ProfileScreenTitle_TV() {
        return ActionHelper.getText(profileScreenTitle_TV);
    }

    public String getText_ScreenTitle_TV() {
        return ActionHelper.getText(screenTitle_TV);
    }

    public String getText_ScreenHeading_TV() {
        return ActionHelper.getText(screenHeading_TV);
    }

    public void click_editProfile_RL() {
        ActionHelper.gotoSleep(2000);
        ActionHelper.scrollTillElementVerySlowly(editProfile_RL);
        ActionHelper.click(editProfile_RL);
        ActionHelper.waitForPageProgress();
    }

    public boolean isPresent_UserName_TV() {
        return ActionHelper.isPresentWithWait(nameProfile_TV);
    }

    public boolean isPresent_QrCode_IV() {
        return ActionHelper.isPresent(qrCode_IV);
    }

    public void click_LogoutButton_Btn() {
        ActionHelper.scrollTillElementSlowly(logoutButton_Btn);
        ActionHelper.click(logoutButton_Btn);
    }

    public String getText_SavedCardname_TV(){
        return ActionHelper.getText(savedCardname_TV);
    }
}
