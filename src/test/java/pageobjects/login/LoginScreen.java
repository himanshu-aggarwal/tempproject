package pageobjects.login;

import com.one97.paytm.appautomation.driver.DriverManager;
import com.one97.paytm.appautomation.utils.ActionHelper;
import org.openqa.selenium.By;

public class LoginScreen {

    private static LoginScreen instance = new LoginScreen();

    private By loginPageTitle_TV = By.id("net.one97.paytm:id/login_button");
    private By email_ET = By.id("net.one97.paytm:id/et_registered_mobile");
    private By password_ET = By.id("net.one97.paytm:id/et_login_password");
    private By secureLoginButton_TV = By.id("net.one97.paytm.cst:id/lyt_sign_in_button");
    private By doItLaterButton_TV = By.id("net.one97.paytm:id/login_preview_skip");
    private By backbutton_IB = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    private By troubleLoggingIn_TV = By.id("net.one97.paytm:id/signin_login_issues");
    private By loginIssues_RL = By.id("net.one97.paytm:id/login_issue_layout");
    private By loginIssues_MobileNumber_ET = By.id("net.one97.paytm.cst:id/text_input_email");
    private By loginToPaytm_Btn = By.id("net.one97.paytm:id/login_button");
    private By proceedLogin_Btn = By.id("net.one97.paytm:id/proceed_btn_login");
    private By proceedLoginPassword_Btn = By.id("net.one97.paytm:id/proceed_btn_login_password");
    private By skip_Btn = By.xpath("//android.widget.TextView[@text='Skip']");

    public static LoginScreen getInstance() {
        return instance;
    }

    public void fill_Email_ET(String email) {
        ActionHelper.fillWithClear(email_ET, email);
        click_proceedLogin_Btn();
    }

    public void fill_Password_ET(String password) {
        ActionHelper.fill(password_ET, password);
        click_proceedLoginPassword_Btn();
    }

    public void click_loginPaytm_Btn() {
        ActionHelper.waitForPageProgress();
        if (ActionHelper.isPresent(loginToPaytm_Btn))
            ActionHelper.click(loginToPaytm_Btn);
    }

    public void click_proceedLogin_Btn() {
        ActionHelper.click(proceedLogin_Btn);
        ActionHelper.waitForPageProgress();
    }

    public void click_proceedLoginPassword_Btn() {
        ActionHelper.click(proceedLoginPassword_Btn);
        ActionHelper.waitForPageProgress();
    }

    public boolean isDisplayed_LoginPageTitle_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(loginPageTitle_TV);
    }

    public boolean isDisplayed_LoginPaytm_Btn() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(loginToPaytm_Btn);
    }

    public boolean isDisplayed_Email_ET() {
        return ActionHelper.isPresent(email_ET);
    }

    public void click_doItLaterButton_TV() {
        ActionHelper.gotoSleep(2000);
        try {
            ActionHelper.click(doItLaterButton_TV);
        }catch (Exception e){
            ActionHelper.click(skip_Btn);
        }
    }

    public void click_Backbutton_IB() {
        ActionHelper.click(backbutton_IB);
    }

    public void click_SecureLoginButton_TV() {
        ActionHelper.click(secureLoginButton_TV);
        ActionHelper.waitForPageProgress();
    }

    public void performLogin(String email, String password) {
        click_loginPaytm_Btn();
        fill_Email_ET(email);
        fill_Password_ET(password);
        ActionHelper.gotoSleep(3000);
    }

    public void click_TroubleLoggingIn_TV() {
        ActionHelper.click(troubleLoggingIn_TV);
    }

    public void click_LoginIssues_RL() {
        ActionHelper.click(loginIssues_RL);
    }

    public void fill_LoginIssues_MobileNumber_ET() {
        ActionHelper.fill(loginIssues_MobileNumber_ET, DriverManager.getMobileNumber());
    }

    public boolean isDisplayed_LoginIssues_MobileNumber_ET() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(loginIssues_MobileNumber_ET);
    }

}
