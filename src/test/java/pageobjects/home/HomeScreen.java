package pageobjects.home;

import com.one97.paytm.appautomation.driver.DriverManager;
import com.one97.paytm.appautomation.enums.MatchType;
import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.logging.Logger;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import pageobjects.shared.FlyOutMenu;
import pageobjects.shared.SharedActions;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class HomeScreen {

    public static By flyoutButton_TV = By.xpath("//android.view.ViewGroup[@resource-id='net.one97.paytm:id/common_toolbar']/android.widget.ImageButton");
    private static HomeScreen instance = new HomeScreen();

    //Header
    private By searchBar_RL = By.id("net.one97.paytm:id/contain");
    private By searchTextBox_ET = By.id("net.one97.paytm:id/search_src_text");
    private String firstItem_SearchResults_RL = "//android.widget.TextView[contains(@resource-id,'net.one97.paytm:id/search_item_text') and contains(@text,'xyz') or contains(@text,'lowercase') or contains(@text,'firstcaptital')]";
    private By firstItem_SearchResults_TV = By.id("net.one97.paytm:id/popular_search_item_text");
    private By backButton_IB = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
    private By notificationIcon_TV = By.id("net.one97.paytm:id/tool_bar_noti");
    private By notificationCount_TV = By.id("net.one97.paytm:id/txt_new_notification");
    private By paytmLogo_TV = By.id("net.one97.paytm:id/paytm_logo");
    private By cashBackIcon_IV = By.xpath("//android.view.ViewGroup[@resource-id='net.one97.paytm:id/common_toolbar']//android.widget.ImageView[@resource-id='net.one97.paytm:id/ic_profile']");
    private By indicatorLayout_TV = By.id("net.one97.paytm:id/indicator_layout");
    private By banners_TV = By.id("net.one97.paytm:id/viewpager_image");
    private By offerArrow_TV = By.id("net.one97.paytm:id/arrow_ryt");
    private By offerDesc_TV = By.id("net.one97.paytm:id/top_bar_wallet_yellow_tv");

    //payments Section
    private By paymentsSectionLayout_LL = By.id("net.one97.paytm:id/ll_blue_header_wrapper");
    private By elementList_ME = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name')]/..");
    private By pay_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Pay']");
    private By moneyTransfer_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and contains(@text,'Transfer')]");
    private By passbook_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Passbook']");
    private By addMoney_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Add Money']");
    private By upi_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='UPI']");
    private By acceptPayment_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Accept Payment']");
    private By lifafa_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Lifafa']");
    private By nearby_KYC_Point_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Nearby KYC Point']");
    private By FavouriteStore_Btn = By.xpath("//android.widget.TextView[contains(@resource-id,'item_name') and @text='Favourite Stores']");


    //Recharges
    // private By viewAll_RL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/view_txt' and @text='View All']/..");
    private By more_RL = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/grid_text_1' and @text='More']/..");
    private By allsmartIcons_HomeScreen_ME = By.xpath("//*[@resource-id='net.one97.paytm:id/grid_container_1']//android.widget.TextView");
    private String smartIcons_Xpath_ME = "//*[@resource-id='net.one97.paytm:id/grid_container_1']//android.widget.TextView[contains(@text,'xyz')]";
    private String smartIconsExact_Xpath_ME = "//*[@resource-id='net.one97.paytm:id/grid_container_1']//android.widget.TextView[@text='xyz']";


    //Super Value Deals
    private By firstItem_SuperValueDealsMall_LL = By.id("net.one97.paytm:id/store_product_row_item_lyt");
    private By viewAll_GroceryEssensialsMall_TV = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/brand_name' and @text='Groceries Essentials']/../../../android.widget.TextView[@text='View All']");
    private By smileIcon_IV = By.id("net.one97.paytm:id/ic_profile");

    //
    private By categoryList_C1C2Banner_IV = By.xpath("//android.widget.ImageView[contains(@resource-id,'net.one97.paytm:id/banner_2xn')]");
    private By screenHeading_TV = By.xpath("//android.view.ViewGroup[@resource-id='net.one97.paytm:id/toolbar']/android.widget.TextView");
    private String searchResults_Xpath_ME = "//android.widget.TextView[contains(@text,'xyz')]";
    private By help_TV = By.id("net.one97.paytm:id/tv_contact_us");
    private By paymentReminders_TV = By.xpath("//android.widget.TextView[@text='Payment Reminders']");
    private By viewAllReminders_TV = By.xpath("//android.widget.TextView[@text='View All Reminders']");
    private By sorryForInconvenience = By.xpath("//android.widget.TextView[contains(@text,'Sorry for the Inconvenience') or contains(@text,'Sorry for the inconvenience') or contains(@content-desc,'Sorry for the inconvenience')]");
    private By okButton_ConnectionErrorLayer_Btn = By.xpath("//android.widget.Button[@resource-id='net.one97.paytm:id/w_custom_dialog_btn_positive' and @text='OK']");
    private By nearByStores_TV = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/updates_title_text' and @text='Nearby Stores']");

    private HomeScreen() {
    }

    public static HomeScreen getInstance() {
        Logger.logPass("On Home Page");
        return instance;
    }

    public void goBackToHomeScreen() {
        while (!ActionHelper.isPresent(more_RL)) {
            ActionHelper.goBack();
            ActionHelper.waitForPageProgress();
            ActionHelper.gotoSleep(500);
        }
        Logger.logPass("Pressing Back- Going Back to Home Screen");
    }

    public void click_FlyoutButton_TV() {
        ActionHelper.click(flyoutButton_TV);
        ActionHelper.gotoSleep(1000);
    }

    public void navigateToProfileScreen() {
        HomeScreen.getInstance().click_FlyoutButton_TV();
        FlyOutMenu.getInstance().click_ProfileName_TV();
    }

    public By SmartIcons_ME(String categoryName) {
        By by = By.xpath(smartIcons_Xpath_ME.replace("xyz", categoryName));
        return by;

    }

    public By SmartIconsExactMatch_ME(String categoryName) {
        By by = By.xpath(smartIconsExact_Xpath_ME.replace("xyz", categoryName));
        return by;

    }

    public void click_SearchResults_Xpath_ME(String searchItem) {
        By by = By.xpath(searchResults_Xpath_ME.replace("xyz", searchItem));
        ActionHelper.click(by);
        ActionHelper.waitForPageProgress();

    }

    public boolean isDisplayed_SmartIcons_ME(String categoryame, MatchType matchType) {
        if (matchType == MatchType.PARTIAL) {
            By by = SmartIcons_ME(categoryame);
            return ActionHelper.isPresent(by);
        } else {
            By by = SmartIconsExactMatch_ME(categoryame);
            return ActionHelper.isPresent(by);
        }
    }

    public void click_SmartIcons_ME(String categoryame, MatchType matchType) {
        By by;
        if (matchType == MatchType.PARTIAL) {
            by = SmartIcons_ME(categoryame);
        } else {
            by = SmartIconsExactMatch_ME(categoryame);
        }
        ActionHelper.click(by);
        ActionHelper.waitForPageProgress();
        ActionHelper.hideKeyboard();
    }

    public void fill_SearchTextBox_ET(String input) {
        ActionHelper.fill(searchTextBox_ET, input);
        ActionHelper.gotoSleep(5000);
    }

    public boolean isPresent_SearchTextBox_ET() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresent(searchTextBox_ET);
    }

    public void click_FirstItem_SearchResults_RL(String productName) {
        String lowercase = productName.toLowerCase();
        String firstLetterCaptial = StringUtils.capitalize(productName);
        By by = By.xpath(firstItem_SearchResults_RL.replace("lowercase", lowercase).replace("firstcaptital", firstLetterCaptial).replace("xyz", productName));
        ActionHelper.click(by);
        ActionHelper.waitForPageProgress();
    }

    public void click_SearchBar_RL() {
        ActionHelper.click(searchBar_RL);
    }

    public void click_Pay_Btn() {
        ActionHelper.click(pay_Btn);
        SharedActions.getInstance().click_Close_SecurePatternLayer_Btn();

    }

    public boolean isDisplayed_Pay_Btn() {
        return ActionHelper.isPresent(pay_Btn);
    }

    public boolean isDisplayed_MoneyTransfer() {
        return ActionHelper.isPresent(moneyTransfer_Btn);
    }

    public boolean isDisplayed_Paasbook() {
        return ActionHelper.isPresent(passbook_Btn);
    }

    public boolean isDisplayed_AddMoney() {
        return ActionHelper.isPresent(addMoney_Btn);
    }

    public void click_MoneyTransfer_Btn() {
        scrollP2PStripHorizontally(moneyTransfer_Btn);
        ActionHelper.click(moneyTransfer_Btn);
    }

    public void click_Passbook_Btn() {
        scrollP2PStripHorizontally(passbook_Btn);
        ActionHelper.click(passbook_Btn);
        SharedActions.getInstance().click_Close_SecurePatternLayer_Btn();
    }

    public void click_AddMoney_Btn() {
        scrollP2PStripHorizontally(addMoney_Btn);
        ActionHelper.click(addMoney_Btn);
    }

    public void click_UPI_Btn() {
        scrollP2PStripHorizontally(upi_Btn);
        ActionHelper.click(upi_Btn);
    }

    public void click_BackButton_IB() {
        ActionHelper.click(backButton_IB);
    }

    public void click_AcceptPayment_Btn() {
        scrollP2PStripHorizontally(acceptPayment_Btn);
        ActionHelper.click(acceptPayment_Btn);
    }

    public void click_Lifafa_Btn() {
        boolean found = scrollP2PStripHorizontally(lifafa_Btn);
        if (found) {
            ActionHelper.click(lifafa_Btn);
        } else {
            HomeScreen.getInstance().click_Category("Lifafa", MatchType.PARTIAL);
        }
    }

    public void click_FavouriteStore_Btn() {
        boolean found = scrollP2PStripHorizontally(FavouriteStore_Btn);
        if (found) {
            ActionHelper.click(FavouriteStore_Btn);
        } else {
            BottomNavigationBar.getInstance().click_Tab_BNB_RL("Inbox");
            ActionHelper.click(nearByStores_TV);

        }
    }


    public boolean isDisplayed_Nearby_KYC_Point_Btn() {
        scrollP2PStripHorizontally(nearby_KYC_Point_Btn);
        return ActionHelper.isPresent(nearby_KYC_Point_Btn);
    }

    public List<String> getNames_allSmartIcons_HomePage_ME() {
        List<String> nameList = new ArrayList<>();
        List<MobileElement> mobileElementList = ActionHelper.findElements(allsmartIcons_HomeScreen_ME);
        for (MobileElement me : mobileElementList) {
            nameList.add(me.getText());
        }
        return nameList;
    }

    public void click_Nearby_KYC_Point_Btn() {
        scrollP2PStripHorizontally(nearby_KYC_Point_Btn);
        ActionHelper.click(nearby_KYC_Point_Btn);
    }

    public boolean scrollP2PStripHorizontally(By by) {
        boolean elementFound = false;
        if (ActionHelper.isPresent(by)) {
            return true;
        }
        int y = ActionHelper.findElement(elementList_ME).getCenter().y;
        int width = DriverManager.getDriver().manage().window().getSize().getWidth();
        double startX = width * 0.85;
        double endX = width * 0.15;
        TouchAction action = new TouchAction(DriverManager.getDriver());
        try {
            action.press(PointOption.point((int) endX, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                    .moveTo(PointOption.point((int) startX, y)).release().perform();
            action.press(PointOption.point((int) endX, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                    .moveTo(PointOption.point((int) startX, y)).release().perform();
        } catch (Exception e) {
        }

        y = ActionHelper.findElement(elementList_ME).getCenter().y;
        width = DriverManager.getDriver().manage().window().getSize().getWidth();
        startX = width * 0.80;
        endX = width * 0.20;
        action = new TouchAction(DriverManager.getDriver());
        int i = 0;
        while (i < 3) {
            if (ActionHelper.isPresent(by)) {
                elementFound = true;
                break;
            }
            action.press(PointOption.point((int) startX, y)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
                    .moveTo(PointOption.point((int) endX, y)).release().perform();
            i++;
        }
        return elementFound;
    }

    public void click_FirstItem_SuperValueDealsMall_LL() {
        ActionHelper.scrollTillElementSlowly(firstItem_SuperValueDealsMall_LL);
        ActionHelper.click(firstItem_SuperValueDealsMall_LL);
    }

    public void click_ViewAll_GroceryEssensialsMall_TV() {
        ActionHelper.scrollTillElementSlowly(viewAll_GroceryEssensialsMall_TV);
        ActionHelper.click(viewAll_GroceryEssensialsMall_TV);
    }

    public void click_More_RL() {
        ActionHelper.hideKeyboard();
        ActionHelper.click(more_RL);
        ActionHelper.waitForPageProgress();
    }

    public String getText_FirstItem_SearchResults_TV() {
        return ActionHelper.getText(firstItem_SearchResults_TV);
    }

    public void click_Category_C1C2Banner_IV(String category) {
        ActionHelper.scrollTillElementSlowly(categoryList_C1C2Banner_IV);
        int i = 0;
        boolean flag = false;
        while (i < 3) {
            List<MobileElement> meList = ActionHelper.findElements(categoryList_C1C2Banner_IV);

            for (MobileElement me : meList) {
                me.click();
                if (ActionHelper.getText(screenHeading_TV).toLowerCase().contains(category)) {
                    flag = true;
                    break;
                } else {
                    ActionHelper.navigateBack();
                }
            }

            if (flag)
                break;

            ActionHelper.scrollPageDownSlowly();
        }
    }

    public void click_Help_TV() {
        ActionHelper.scrollTillElement(help_TV);
        ActionHelper.click(help_TV);
    }

    public boolean isDisplayed_PaymentReminders_TV() {
        ActionHelper.scrollTillElementVerySlowly(paymentReminders_TV);
        return ActionHelper.isPresent(paymentReminders_TV);
    }

    public boolean isDisplayed_ViewAllReminders_TV() {
        ActionHelper.scrollTillElementVerySlowly(viewAllReminders_TV);
        return ActionHelper.isPresent(viewAllReminders_TV);
    }

    public void click_Category(String value, MatchType matchType) {
        ActionHelper.hideKeyboard();

        if (ActionHelper.isPresent(sorryForInconvenience)) {
            ActionHelper.click(okButton_ConnectionErrorLayer_Btn);
        }

        if (matchType == MatchType.DEEPLINK) {
            ActionHelper.openDeeplink(value);
        } else {
            if (isDisplayed_SmartIcons_ME(value, matchType)) {
                click_SmartIcons_ME(value, matchType);
            } else {
                click_More_RL();
                BrowseCategoriesScreen.getInstance().click_Category(value, matchType);
            }
        }
    }

    public void click_NotificationIcon_TV() {
        ActionHelper.click(notificationIcon_TV);
    }

    public String getText_NotificationCount_TV() {
        return ActionHelper.getText(notificationCount_TV);
    }

    public boolean isPresent_PaytmLogo_TV() {
        ActionHelper.gotoSleep(500);
        return ActionHelper.isPresent(paytmLogo_TV);
    }

    public boolean isPresent_IndicatorLayout_TV() {
        return ActionHelper.isPresent(indicatorLayout_TV);
    }

    public boolean isPresent_Banners_TV() {
        return ActionHelper.isPresent(banners_TV);
    }

    public void click_Banners_TV() {
        ActionHelper.click(banners_TV);
        ActionHelper.waitForPageProgress();

    }

    public void click_CashBackIcon_IV() {
        ActionHelper.click(cashBackIcon_IV);
    }

    public void click_OfferDesc_TV() {
        ActionHelper.click(offerDesc_TV);
        ActionHelper.waitForPageProgress();

    }

    public void click_OfferArrow_TV() {
        ActionHelper.click(offerArrow_TV);
        ActionHelper.waitForPageProgress();
    }

}
