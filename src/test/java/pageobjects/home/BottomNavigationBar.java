package pageobjects.home;

import com.one97.paytm.appautomation.utils.ActionHelper;
import org.openqa.selenium.By;
import pageobjects.shared.SharedActions;

public class BottomNavigationBar {

    private static BottomNavigationBar instance = new BottomNavigationBar();
    public String tabsList_Xpath_RL = "//android.widget.TextView[@resource-id='net.one97.paytm:id/tab_txt' and contains(@text,'xyz')]/..";
    private By paymentbank_TV = By.xpath("//android.widget.TextView[contains(@text,'India’s most sincere\n" + "bank is here!')]");
    private By inboxTitle = By.xpath("//android.widget.TextView[@resource-id='net.one97.paytm:id/updates_title_text' and contains(@text,'SMS') or contains(@text,'Games') or contains(@text,'News')]");
    private By paytmMall_IconCart = By.id("net.one97.paytm:id/icon_cart");
    private By paytmMall_ActionSearch = By.id("net.one97.paytm:id/action_search");
    private By scanPaytm_TV = By.id("net.one97.paytm:id/status_view");
    private By scanPaytmCode_TV = By.id("net.one97.paytm:id/show_otp_tv");
    private By scanPaytmCloseIcon_TV = By.id("net.one97.paytm:id/iv_close_icon");
    private By inboxLangSelectionClose_IV = By.id("net.one97.paytm.feed:id/close");
    private By inboxLangSelction_Tv = By.id("net.one97.paytm.feed:id/feed_language_header");

    private BottomNavigationBar() {
    }

    public static BottomNavigationBar getInstance() {
        return instance;
    }

    public void click_Tab_BNB_RL(String tab) {
        ActionHelper.click(tabsList_Xpath_RL, tab);
        ActionHelper.waitForPageProgressBar(By.id("net.one97.paytm:id/pb_download"));

    }

    public void click_ScanTab_BNB_RL(String tab) {
        ActionHelper.click(tabsList_Xpath_RL, tab);
        ActionHelper.waitForPageProgress();
    }

    public boolean isDisplayed_BankLinkTitle_TV() {
        ActionHelper.waitForPageProgress();
        return ActionHelper.isPresentWithWait(paymentbank_TV);
    }

    public boolean isDisplayed_InboxLink_TV() {
        return ActionHelper.isPresentWithWait(inboxTitle);
    }

    public boolean isDisplayed_PaytmMall_TV() {
        boolean paytmMallHome = false;
        if (ActionHelper.isPresent(paytmMall_IconCart) || ActionHelper.isPresent(paytmMall_ActionSearch))
            paytmMallHome = true;
        return paytmMallHome;
    }

    public boolean isDisplayed_PaytmHome_TV() {
        ActionHelper.waitForPageProgress();
        boolean paytmHome = false;
        HomeScreen homeScreen = HomeScreen.getInstance();
        if (homeScreen.isDisplayed_Pay_Btn() || homeScreen.isDisplayed_AddMoney() || homeScreen.isDisplayed_MoneyTransfer() || homeScreen.isDisplayed_Paasbook())
            paytmHome = true;
        return paytmHome;
    }

    public boolean isDisplayed_ScanLink_TV() {
        ActionHelper.waitForPageProgress();
        SharedActions.getInstance().click_AllowButton_PermissionDialogue_Btn();
        if (ActionHelper.isPresent(scanPaytmCloseIcon_TV)) {
            ActionHelper.click(scanPaytmCloseIcon_TV);
        }
        boolean scan = false;
        HomeScreen homeScreen = HomeScreen.getInstance();
        if (ActionHelper.isPresent(scanPaytm_TV) || ActionHelper.isPresent(scanPaytmCode_TV))
            scan = true;
        return scan;
    }

    public void click_PaytmMall_ActionSearch() {
        ActionHelper.click(paytmMall_ActionSearch);
    }


    public void close_lang_selection_Inbox() {
        if (ActionHelper.isPresent(inboxLangSelction_Tv)) {
            ActionHelper.click(inboxLangSelectionClose_IV);
        }
    }
}
