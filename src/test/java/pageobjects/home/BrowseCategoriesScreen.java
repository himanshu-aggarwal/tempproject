package pageobjects.home;

import com.one97.paytm.appautomation.enums.MatchType;
import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.logging.Logger;
import org.openqa.selenium.By;

public class BrowseCategoriesScreen {

    private static BrowseCategoriesScreen instance = new BrowseCategoriesScreen();
    private By searchButton_IV = By.id("net.one97.paytm:id/search_button");
    private By searchCategory_EditText_ET = By.id("net.one97.paytm:id/edit_search");
    private By allCategories_Title_TV = By.id("net.one97.paytm:id/booking_recharge_title");
    private String categoryList_Xpath_ME = "//android.widget.TextView[@text='xyz']/..";
    private String categoryList_Contains_Xpath_ME = "//android.widget.TextView[contains(@text,'xyz')]/..";
    private By backButton_IM = By.id("net.one97.paytm:id/iv_back_arrow");

    private BrowseCategoriesScreen() {
    }

    public static BrowseCategoriesScreen getInstance() {
        Logger.logPass("On Browse Category Page");
        return instance;
    }

    public void click_SearchButton_IV() {
        ActionHelper.click(searchButton_IV);
    }

    public void click_BackButton_IM() {
        ActionHelper.click(backButton_IM);
    }

    public void fill_SearchCategory_EditText_ET(String input) {
        ActionHelper.fill(searchCategory_EditText_ET, input);
    }

    public void click_AllCategories_Title_TV() {
        ActionHelper.click(allCategories_Title_TV);
    }

    public boolean isDisplayed_CategoryList_Contains_Xpath_ME(String categoryName) {
        By by = By.xpath(categoryList_Contains_Xpath_ME.replace("xyz", categoryName));
        return ActionHelper.isPresent(by);
    }

    public void click_Category_PartialMatch_ME(String categoryName) {
        try {
            while (ActionHelper.isPresent(allCategories_Title_TV)) {
                fill_SearchCategory_EditText_ET(categoryName);
                ActionHelper.clickWithScroll(categoryList_Contains_Xpath_ME, categoryName);
            }
            ActionHelper.waitForPageProgress();
            ActionHelper.hideKeyboard();
        } catch (Exception e) {
            System.out.println("Exception in Category");
            ActionHelper.waitForPageProgress();
            ActionHelper.hideKeyboard();
        }
    }

    public void click_Category_ExactMatch_ME(String categoryName) {
        try {
            while (ActionHelper.isPresent(allCategories_Title_TV)) {
                fill_SearchCategory_EditText_ET(categoryName);
                ActionHelper.clickWithScroll(categoryList_Xpath_ME, categoryName);
            }
            ActionHelper.waitForPageProgress();
            ActionHelper.hideKeyboard();
        } catch (Exception e) {
            System.out.println("Exception in Category");
            ActionHelper.waitForPageProgress();
            ActionHelper.hideKeyboard();
        }
    }

    public void click_Category(String category, MatchType matchType) {
        if (matchType == MatchType.PARTIAL) {
            BrowseCategoriesScreen.getInstance().click_Category_PartialMatch_ME(category);
        } else {
            BrowseCategoriesScreen.getInstance().click_Category_ExactMatch_ME(category);
        }
    }
}

