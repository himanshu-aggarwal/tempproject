package testsuite.addmoney;

import org.testng.annotations.Test;
import pageobjects.addmoney.AddMoneyScreen;
import pageobjects.addmoney.WalletAddMoneyScreen;
import pageobjects.home.HomeScreen;
import pageobjects.shared.SharedActions;
import testsuite.basetest.AndroidBaseTestClass;

public class ExecutionSuite_AddMoney extends AndroidBaseTestClass {

    @Test(groups = {"regression", "mobile","sanity","smoke"},
            description = "To Validate that Mobile Prepaid functionality by without promocode and Deals")
    public void testingCase(){
        SharedActions.getInstance().performCommonActions();
        HomeScreen homeScreen = HomeScreen.getInstance();
        homeScreen.click_AddMoney_Btn();
        AddMoneyScreen addMoneyScreen = AddMoneyScreen.getInstance();
        addMoneyScreen.click_AddMoneyToWallet();
        WalletAddMoneyScreen walletAddMoneyScreen = WalletAddMoneyScreen.getInstance();
        walletAddMoneyScreen.inputAddMoneyAmount("10");
        walletAddMoneyScreen.click_AddMoneyButton();
        System.out.println("ALL steps done");


    }


}
