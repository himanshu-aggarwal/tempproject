package testsuite.basetest;

import com.one97.paytm.appautomation.android.GenericFunctionsAndroid;
import com.one97.paytm.appautomation.annotations.RunOnMobileNumber;
import com.one97.paytm.appautomation.dataobjects.DeviceDTO;
import com.one97.paytm.appautomation.driver.DriverManager;
import com.one97.paytm.appautomation.global.GlobalData;
import com.one97.paytm.appautomation.utils.ActionHelper;
import com.one97.paytm.reporting.constants.Constants;
import com.one97.paytm.reporting.extentreports.ExtentManager;
import com.one97.paytm.reporting.listeners.TestNGReportListener;
import com.one97.paytm.reporting.utils.TestStatusListener;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import pageobjects.shared.SharedActions;

import java.util.HashMap;

public class InvocationListener implements IInvokedMethodListener, TestStatusListener {

    {
        TestNGReportListener.initStatusListener(this);
    }


    @Override
    public synchronized void beforeInvocation(IInvokedMethod method, ITestResult result) {
        if (method.isTestMethod()) {
            DeviceDTO deviceDTO;

            if (checkBindingwithMobileNumber(result))
                deviceDTO = waitForParticularDevice(result);
            else
                deviceDTO = GlobalData.devicePool.remove();


            DriverManager.setDeviceDTO(deviceDTO);
            ExtentManager.setDriver(deviceDTO.getDriver());
            ExtentManager.addDeviceInfoToReport(deviceDTO.getDeviceDetails());
            if (result.getParameters().length > 0 && "false".equalsIgnoreCase(((HashMap<String, String>) result.getParameters()[0]).get(Constants.KEY_LAUNCH_APP))) {
                ActionHelper.launchApp();
            } else {
                ActionHelper.launchApp();
            }

        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult result) {
        if (method.isTestMethod()) {
            if (result.getStatus() == ITestResult.FAILURE && !result.getThrowable().toString().contains("exception.SkipTestException")) {
                SharedActions.getInstance().checkSessionAndPerformLogin();
                GenericFunctionsAndroid genericFunctionsAndroid = new GenericFunctionsAndroid();
                genericFunctionsAndroid.addADBLogsInReport(result);
            }
            GlobalData.devicePool.add(DriverManager.getDeviceDTO());
            System.gc();
        }

    }

    private synchronized boolean checkBindingwithMobileNumber(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(RunOnMobileNumber.class) != null;
    }

    private synchronized DeviceDTO waitForParticularDevice(ITestResult result) {
        String mobileNumber = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(RunOnMobileNumber.class).value();
        int count = 0;

        DeviceDTO deviceDTO;

        while (count < 240) {
            deviceDTO = GlobalData.devicePool.remove();
            System.out.println(result.getName() + " is waiting current mob no is = " + deviceDTO.getMobileNumber());

            if (deviceDTO.getMobileNumber().equals(mobileNumber))
                return deviceDTO;

            GlobalData.devicePool.add(deviceDTO);
            count++;
            ActionHelper.gotoSleep(500);
        }

        return GlobalData.devicePool.remove();
    }


    @Override
    public void performOnSuccess(ITestResult result) {
    }

    @Override
    public void performOnFailure(ITestResult result) {
        DriverManager.setLoggedIn(false);
    }

    @Override
    public void performOnSkip(ITestResult result) {

    }
}
