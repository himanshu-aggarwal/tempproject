package testsuite.basetest;

import com.one97.paytm.appautomation.android.GenericFunctionsAndroid;
import com.one97.paytm.appautomation.driver.DriverManager;
import com.one97.paytm.appautomation.factory.GenericFactory;
import com.one97.paytm.appautomation.global.GlobalData;
import com.one97.paytm.appautomation.utils.GenericFunctions;
import com.one97.paytm.reporting.listeners.TestNGReportListener;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import pageobjects.shared.SharedActions;
import static com.one97.paytm.appautomation.global.GlobalData.devicePool;
import static global.PaymentsGlobalData.*;

@Listeners({InvocationListener.class, TestNGReportListener.class})
public class AndroidBaseTestClass {
    public GenericFunctions genericFunctions;


    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        System.out.println("--------------------- INSIDE BEFORE SUITE ---------------------");
        initGenericFunctions();
        initExtentReporter();
/*
        initZephyrReporter();
        initKlovReporter();
*/
        initAppiumDrivers();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        System.out.println("--------------------- INSIDE AFTER SUITE ---------------------");
        fetchAndAddAppDetailsInReport();
        updateDetailsExtentReports();
        updateResultsToDing();
        quitAppiumDrivers();
    }


    private void fetchAndAddAppDetailsInReport() {
        DriverManager.setDeviceDTO(devicePool.remove());
        SharedActions.getInstance().fetchAndSetAppVersion_Stamp();
        devicePool.add(DriverManager.getDeviceDTO());
    }


    private void initGenericFunctions() {
        genericFunctions = GenericFactory.getGenericFunctions(NODE_PATH, APPIUM_JS_FILE_PATH, LANGUAGE, APP_COMPLETE_PATH, ADBPATH,
                APP_PACKAGE, APP_ACTIVITY);
    }

    private void initExtentReporter() {
        genericFunctions.initDirectories();
        genericFunctions.initExtentReporter(EXTENTREPORT_DOCUMENTTITLE, EXTENTREPORT_REPORTNAME, FLAG_UPDATEJIRA, FLAG_CAPTURESCREENSHOTS,
                FLAG_REMOVE_RETRIEDTESTS);
    }


    private void initZephyrReporter() {
        if (FLAG_UPDATEJIRA)
            genericFunctions.initZephyrReporter(JIRA_USER_NAME, JIRA_PASSWORD, JIRA_PROJECT_NAME, JIRA_RELEASE_NAME_PREFIX,
                    JIRA_TEST_REPO_RELEASE_NAME, JIRA_TEST_REPO_CYCLE_NAME, JIRA_NEW_CYCLE_NAME_PREFIX);
    }

    private void initKlovReporter() {
        if (FLAG_UPDATEKLOV)
            genericFunctions.initKlovReporter(KLOV_PROJECTNAME, KLOV_SERVERIP, KLOV_MONGODBPORT, KLOV_SERVERPORT);
    }

    private void updateResultsToDing() {
        if (FLAG_UPDATEDING)
            genericFunctions.pushResultsToDingGroup(DING_ACCESS_TOKEN_AUTOMATIONGROUP, DING_MESSAGETITLE,
                    DING_THRESHOLDPERCENTAGE, DING_ANDROIDLOGO_URL, JENKINS_REPORT_URL, APPSTAMP);
    }

    private void initAppiumDrivers() {
        try {
            genericFunctions.initAppiumServers_Drivers();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private void quitAppiumDrivers() {
        genericFunctions.closeAppAllDevices();
        genericFunctions.killAppiumServers();
    }

    private void updateDetailsExtentReports() {
        genericFunctions.addExecutionDetails_ExtentReport(APPSTAMP, ENVIRONMENT, EXECUTION_TYPE, GROUP_NAMES, EXCLUDE_GROUPS, LANGUAGE);
    }




}
